## Lotus Data structures - Heaps

This is part of the Lotus data structure projects and contains implementations of the Heap data 
structures.

Current supports

1. Max Heap
2. Min Heap

Other heap data structures will be added in the coming time

### import to your project

import the dependency from the maven central database

    TBD


Once the maven dependencies are in place you can create the heap in two ways
1. with initial size - this will not restrict the heap to the size, but will start with initial 
storage size allocated thus reducing the overhead of increasing the size later on the fly

        MaxHeap<Integer> heap = new MaxHeap<>(50);

2. without initial size - this will set the initial value to be 10. but will increase the size as the heap grows

        MaxHeap<Integer> heap = new MaxHeap<>();

standard heap operations are available

1. add
2. poll
3. peek
4. clear


