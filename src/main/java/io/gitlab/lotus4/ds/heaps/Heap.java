package io.gitlab.lotus4.ds.heaps;

import io.gitlab.lotus4.ds.heaps.exception.HeapUnderflowException;
import java.util.Vector;
import org.slf4j.Logger;

/**
 *
 * Abstract Heap data strucuture supporing both max and min heap
 */
abstract class Heap<T extends Comparable> {

  protected Logger logger;

  /**
   * Storage to keep the heap data structure in place
   */
  protected Vector<T> storage;

  /**
   * create the heap data structure with logger and default initial size
   * @param logger logger implementation to be used to log activities
   */
  protected Heap(Logger logger) {
    storage = new Vector<>();
    this.logger = logger;
  }

  /**
   * create the heap data structure with logger and custom initial size
   * @param logger logger implementation to be used to log activities
   * @param capacity - initial capacity
   */
  protected Heap(int capacity, Logger logger) {
    storage = new Vector<>(capacity);
    this.logger = logger;
  }


  /**
   * Returns the size of the heap
   * @return - size of the heap
   */
  public int size() {
    return storage.size();
  }

  /**
   * Returns whether or not the heap is empty
   * @return - true if empty and falase otherwise
   */
  public Boolean isEmpty() {
    return storage.isEmpty();
  }

  /**
   * Add a new value of type T to the heap. the addition guarantee the resultant heap is still valid
   * heap
   * @param value - new value to be added
   */
  public void add(T value) {
    storage.addElement(value);

    int index = size() - 1;
    heapifyUp(index);
  }

  /**
   * returns the head of the heap while removing it from the heap
   * @return - head of the heap
   * @throws HeapUnderflowException if heap is empty
   */
  public T poll() throws HeapUnderflowException {
    if (size() == 0) {
      throw new HeapUnderflowException("Index out of range (Heap underflow)");
    }
    T root = storage.firstElement();
    storage.setElementAt(storage.lastElement(), 0);
    storage.remove(size() - 1);
    heapifyDown(0);
    return root;
  }

  /**
   * returns the head of the heap without removing it from the heap
   * @return - head of the heap
   * @throws HeapUnderflowException if heap is empty
   */
  public T peek() throws HeapUnderflowException {
    if (size() == 0) {
      throw new HeapUnderflowException("Index out of range (Heap underflow)");
    }
    return storage.firstElement();
  }

  /**
   * clear the heap
   */
  public void clear() {
   logger.info("Emptying queue: ");
    while (!storage.isEmpty()) {
      poll();
    }
    logger.info("Emptying queue completed ");
  }

  /**
   * check if the value passed is present in the heap
   * @param value - value to check
   * @return - true if the value is present and false otherwise
   */
  public Boolean contains(T value) {
    return storage.contains(value);
  }

  protected int parent(int i) {
    // if i is already a root node
    if (i == 0) {
      return 0;
    }

    return (i - 1) / 2;
  }

  protected int left(int i) {
    return (2 * i + 1);
  }

  protected int right(int i) {
    return (2 * i + 2);
  }

  protected void swap(int x, int y) {
    T temp = storage.get(x);
    storage.setElementAt(storage.get(y), x);
    storage.setElementAt(temp, y);
  }

  protected abstract void heapifyDown(int i);

  protected abstract void heapifyUp(int i);

}
