package io.gitlab.lotus4.ds.heaps;

import org.slf4j.LoggerFactory;

/**
 * Min Heap data structure
 * creates a heap data structure with min heap imperatives protected
 */
public final class MinHeap<T extends Comparable> extends Heap<T> {

  /**
   * Creates a min heap without specified initial size. the default size used is 10.
   * Heap with grew as required
   */
  public MinHeap() {
    super(LoggerFactory.getLogger(MinHeap.class));
  }

  /**
   * Creates min heap with initial size set
   * Heap with grew as required
   * @param capacity - initial capacity
   */
  public MinHeap(int capacity) {
    super(capacity, LoggerFactory.getLogger(MinHeap.class));
  }

  protected void heapifyDown(int i) {
    int left = left(i);
    int right = right(i);

    int smallest = i;

    if (left < size() && storage.get(left).compareTo(storage.get(i)) < 0) {
      smallest = left;
    }

    if (right < size() && storage.get(right).compareTo(storage.get(smallest)) < 0) {
      smallest = right;
    }

    if (smallest != i) {
      swap(i, smallest);

      heapifyDown(smallest);
    }
  }

  protected void heapifyUp(int i) {
    if (i > 0 && storage.get(parent(i)).compareTo(storage.get(i)) > 0) {
      swap(i, parent(i));
      heapifyUp(parent(i));
    }
  }
}
