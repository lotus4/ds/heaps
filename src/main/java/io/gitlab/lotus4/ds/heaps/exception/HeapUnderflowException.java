package io.gitlab.lotus4.ds.heaps.exception;

public class HeapUnderflowException extends RuntimeException {

  public HeapUnderflowException(String message) {
    super(message);
  }
}
