package io.gitlab.lotus4.ds.heaps;


import org.slf4j.LoggerFactory;

/**
 * Max Heap data structure
 * creates a heap data structure with max heap imperatives protected
 */
public final class MaxHeap<T extends Comparable> extends Heap<T> {

  /**
   * Creates a max heap without specified initial size. the default size used is 10.
   * Heap with grew as required
   */
  public MaxHeap() {
    super(LoggerFactory.getLogger(MaxHeap.class));
  }

  /**
   * Creates max heap with initial size set
   * Heap with grew as required
   * @param capacity - initial capacity
   */
  public MaxHeap(int capacity) {
    super(capacity, LoggerFactory.getLogger(MaxHeap.class));
  }

  // Recursive Heapify-down procedure. Here the node at index i
  // and its two direct children violates the heap property
  protected void heapifyDown(int i) {
    // get left and right child of node at index i
    int left = left(i);
    int right = right(i);

    int largest = i;

    // compare storage.get(i) with its left and right child
    // and find largest value
    if (left < size() && storage.get(left).compareTo(storage.get(i)) > 0) {
      largest = left;
    }

    if (right < size() && storage.get(right).compareTo(storage.get(largest)) > 0) {
      largest = right;
    }

    if (largest != i) {
      // swap with child having greater value
      swap(i, largest);

      // call heapify-down on the child
      heapifyDown(largest);
    }
  }

  // Recursive Heapify-up procedure
  protected void heapifyUp(int i) {
    // check if node at index i and its parent violates
    // the heap property
    if (i > 0 && storage.get(parent(i)).compareTo(storage.get(i)) < 0) {
      // swap the two if heap property is violated
      swap(i, parent(i));

      // call Heapify-up on the parent
      heapifyUp(parent(i));
    }
  }


}
