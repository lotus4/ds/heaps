package io.gitlab.lotus4.ds.heaps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import io.gitlab.lotus4.ds.heaps.exception.HeapUnderflowException;
import org.testng.annotations.Test;

public class MaxHeapTest {

  @Test
  public void testInteger() {

    MaxHeap<Integer> intMax = new MaxHeap<>();
    intMax.add(1);
    intMax.add(12);
    intMax.add(16);

    assertThat(intMax.peek(),is(16));
    assertThat(intMax.poll(),is(16));
    assertThat(intMax.poll(),is(12));
    assertThat(intMax.poll(),is(1));
  }


  @Test
  public void testObject() {

    MaxHeap<LocalComparable> intMax = new MaxHeap<>();
    intMax.add(new LocalComparable(1));
    intMax.add(new LocalComparable(12));
    intMax.add(new LocalComparable(16));

    assertThat(intMax.peek().getValue(),is(16));
    assertThat(intMax.poll().getValue(),is(16));
    assertThat(intMax.poll().getValue(),is(12));
    assertThat(intMax.poll().getValue(),is(1));
  }


  @Test(expectedExceptions = HeapUnderflowException.class)
  public void tesException() {
    MaxHeap<LocalComparable> intMax = new MaxHeap<>();
    intMax.add(new LocalComparable(1));

    assertThat(intMax.peek().getValue(),is(1));
    intMax.clear();
    intMax.poll();
  }


  class LocalComparable implements Comparable{
    private int value;

    public LocalComparable(int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public void setValue(int value) {
      this.value = value;
    }

    @Override
    public int compareTo(Object o) {
      if(!(o instanceof LocalComparable)){
        throw new IllegalArgumentException("Incorrect value passed to compare");
      }
      LocalComparable casted = (LocalComparable) o;
      return Integer.compare(value, casted.getValue());
    }
  }



}
