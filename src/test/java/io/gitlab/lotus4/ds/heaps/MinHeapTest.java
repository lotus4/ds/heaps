package io.gitlab.lotus4.ds.heaps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import io.gitlab.lotus4.ds.heaps.exception.HeapUnderflowException;
import org.testng.annotations.Test;

public class MinHeapTest {

  @Test
  public void testInteger() {

    MinHeap<Integer> intMin = new MinHeap<>();
    intMin.add(12);
    intMin.add(1);
    intMin.add(16);

    assertThat(intMin.peek(), is(1));
    assertThat(intMin.poll(), is(1));
    assertThat(intMin.poll(), is(12));
    assertThat(intMin.poll(), is(16));
  }


  @Test
  public void testObject() {

    MinHeap<LocalComparable> intMin = new MinHeap<>();
    intMin.add(new LocalComparable(12));
    intMin.add(new LocalComparable(1));
    intMin.add(new LocalComparable(16));

    assertThat(intMin.peek().getValue(), is(1));
    assertThat(intMin.poll().getValue(), is(1));
    assertThat(intMin.poll().getValue(), is(12));
    assertThat(intMin.poll().getValue(), is(16));
  }

  @Test(expectedExceptions = HeapUnderflowException.class)
  public void tesException() {
    MinHeap<LocalComparable> intMin = new MinHeap<>();
    intMin.add(new LocalComparable(1));

    assertThat(intMin.peek().getValue(), is(1));
    intMin.clear();
    intMin.poll();
  }

  class LocalComparable implements Comparable {

    private int value;

    public LocalComparable(int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }

    public void setValue(int value) {
      this.value = value;
    }

    @Override
    public int compareTo(Object o) {
      if (!(o instanceof LocalComparable)) {
        throw new IllegalArgumentException("Incorrect value passed to compare");
      }
      LocalComparable casted = (LocalComparable) o;
      return Integer.compare(value, casted.getValue());
    }
  }


}
